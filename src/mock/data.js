import uuidv1 from 'uuid/v1';
import i18n from '../utils/i18n';

// HEAD DATA
export const headData = {
  title: i18n.t('headData.title'), // e.g: 'Name | Developer'
  lang: i18n.t('headData.lang'), // e.g: en, es, fr, jp
  description: i18n.t('headData.description'), // e.g: Welcome to my website
};

// HERO DATA
export const heroData = {
  title: i18n.t('heroData.title'),
  name: 'Baptiste GOUDEY',
  subtitle: i18n.t('heroData.subtitle'),
  cta: i18n.t('heroData.cta'),
};

// ABOUT DATA
export const aboutData = {
  img: 'profile.jpg',
  paragraphOne: i18n.t('aboutData.paragraphOne'),
  paragraphTwo: i18n.t('aboutData.paragraphTwo'),
  paragraphThree: i18n.t('aboutData.paragraphThree'),
  resume: i18n.t('aboutData.resume'), // if no resume, the button will not show up
};

// PROJECTS DATA
export const projectsData = [
  {
    id: uuidv1(),
    img: 'manadge.jpg',
    title: 'Manadge',
    info: i18n.t('projectsData.manadge.info'),
    info2: i18n.t('projectsData.manadge.info2'),
    url: 'https://manadge.com/',
    repo: '', // if no repo, the button will not show up
  },
  {
    id: uuidv1(),
    img: 'hub-grade.jpg',
    title: 'Hub-grade',
    info: i18n.t('projectsData.hubGrade.info'),
    info2: i18n.t('projectsData.hubGrade.info2'),
    url: 'https://www.hub-grade.com/',
    repo: '', // if no repo, the button will not show up
  },
  {
    id: uuidv1(),
    img: 'memorize.JPG',
    title: 'Memorize',
    info: i18n.t('projectsData.memorize.info'),
    info2: i18n.t('projectsData.memorize.info2'),
    url: 'https://play.google.com/store/apps/details?id=com.goudey.memorize',
    repo: '', // if no repo, the button will not show up
  },
  {
    id: uuidv1(),
    img: 'valpogusto.jpg',
    title: 'ValpoGusto',
    info: i18n.t('projectsData.valpogusto.info'),
    info2: i18n.t('projectsData.valpogusto.info2'),
    url: 'https://www.valpogusto.com/',
    repo: '', // if no repo, the button will not show up
  },
  {
    id: uuidv1(),
    img: 'g2mi.jpeg',
    title: 'G2MI',
    info: i18n.t('projectsData.g2mi.info'),
    info2: i18n.t('projectsData.g2mi.info2'),
    url: 'https://www.g2mi-lyon.com/',
    repo: '', // if no repo, the button will not show up
  },
  {
    id: uuidv1(),
    img: 'blablafoot.JPG',
    title: 'BlaBlaFoot',
    info: i18n.t('projectsData.blablafoot.info'),
    info2: i18n.t('projectsData.blablafoot.info2'),
    url: 'https://play.google.com/store/apps/details?id=com.blablafoot.app',
    repo: '', // if no repo, the button will not show up
  },
  {
    id: uuidv1(),
    img: 'kanopi.JPG',
    title: 'Kanopi',
    info: i18n.t('projectsData.kanopi.info'),
    info2: i18n.t('projectsData.kanopi.info2'),
    url: 'https://fr.kanopi.ch/',
    repo: '', // if no repo, the button will not show up
  },
];

// CONTACT DATA
export const contactData = {
  cta: i18n.t('contactData.cta'),
  btn: i18n.t('contactData.btn'),
  email: 'baptistegoudey@gmail.com',
};

// FOOTER DATA
export const footerData = {
  networks: [
    {
      id: uuidv1(),
      name: 'linkedin',
      url: 'https://www.linkedin.com/in/baptiste-goudey-8a7a71a8/',
    },
    {
      id: uuidv1(),
      name: 'gitlab',
      url: 'https://gitlab.com/baptiste.goudey',
    },
  ],
};

// Github start/fork buttons
export const githubButtons = {
  isEnabled: false, // set to false to disable the GitHub stars/fork buttons
};
