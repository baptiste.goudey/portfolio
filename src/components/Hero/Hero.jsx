import React, { useContext, useState, useEffect } from 'react';
import { Container, Form } from 'react-bootstrap';
import Fade from 'react-reveal/Fade';
import PortfolioContext from '../../context/context';
import i18n from '../../utils/i18n';
import Particles from 'react-tsparticles';

const Header = () => {
  const { hero } = useContext(PortfolioContext);
  const { title, name, subtitle, cta } = hero;

  const [isDesktop, setIsDesktop] = useState(false);
  const [isMobile, setIsMobile] = useState(false);

  const [isShown, setIsShown] = useState(false);

  const [languageSelected, setLanguageSelected] = useState('');

  const changeLanguage = code => {
    if (code === i18n.t('language.en')) i18n.changeLanguage('en');
    else if (code === i18n.t('language.fr')) i18n.changeLanguage('fr');
    setLanguageSelected(code);
    setIsShown(false);
    window.location.reload(false);
  };

  const i18nToLanguage = code => {
    let language = '';
    switch (code) {
      case 'fr':
        language = i18n.t('language.fr');
        break;
      case 'en':
        language = i18n.t('language.en');
        break;
      default:
        break;
    }
    return language;
  };

  useEffect(() => {
    setLanguageSelected(i18nToLanguage(i18n.language));

    setTimeout(() => {
      setIsShown(true);
    }, 1000);

    if (window.innerWidth > 769) {
      setIsDesktop(true);
      setIsMobile(false);
    } else {
      setIsMobile(true);
      setIsDesktop(false);
    }

    return () => {
      setIsShown(false);
    };
  }, []);

  return (
    <section id="hero" className="jumbotron">
      <Form className="select-language">
        <Form.Group controlId="exampleForm.SelectCustom">
          <Form.Label>{i18n.t('language.title')}</Form.Label>
          <Form.Control
            as="select"
            onChange={event => changeLanguage(event.target.value)}
            value={languageSelected}
          >
            <option>{i18n.t('language.fr')}</option>
            <option>{i18n.t('language.en')}</option>
          </Form.Control>
        </Form.Group>
      </Form>
      <Container>
        <Particles
          id="tsparticles"
          options={{
            background: {
              color: {
                value: '#ffffff',
              },
            },
            fpsLimit: 60,
            interactivity: {
              detectsOn: 'canvas',
              events: {
                resize: true,
              },
              modes: {
                bubble: {
                  distance: 400,
                  duration: 2,
                  opacity: 0.8,
                  size: 40,
                },
                push: {
                  quantity: 4,
                },
                repulse: {
                  distance: 200,
                  duration: 0.4,
                },
              },
            },
            particles: {
              color: {
                value: '#0d47a1',
              },
              links: {
                color: '#0d47a1',
                distance: 150,
                enable: true,
                opacity: 0.5,
                width: 1,
              },
              collisions: {
                enable: true,
              },
              move: {
                direction: 'none',
                enable: true,
                outMode: 'bounce',
                random: false,
                speed: 6,
                straight: false,
              },
              number: {
                density: {
                  enable: true,
                  value_area: 800,
                },
                value: 80,
              },
              opacity: {
                value: 0.5,
              },
              shape: {
                type: 'circle',
              },
              size: {
                random: true,
                value: 5,
              },
            },
            detectRetina: true,
          }}
        />
        <Fade left={isDesktop} bottom={isMobile} duration={1000} delay={500} distance="30px">
          <h1 className="hero-title">
            {title} <span className="text-color-main">{name}</span>
            <br />
            {subtitle}
          </h1>
        </Fade>
        <Fade left={isDesktop} bottom={isMobile} duration={1000} delay={1000} distance="30px">
          <p className="hero-cta">
            <span className="background-white" >
              <a className="cta-btn cta-btn--hero" href="#about">
                {cta}
              </a>
            </span>
          </p>
        </Fade>
      </Container>
    </section>
  );
};

export default Header;
