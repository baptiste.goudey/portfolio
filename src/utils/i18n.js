import i18n from "i18next"
import LanguageDetector from "i18next-browser-languagedetector"
import { initReactI18next } from "react-i18next";


import translationEn from "../locales/en/translation.json";
import translationFr from "../locales/fr/translation.json";

i18n
  .use(LanguageDetector)
  .use(initReactI18next)
  .init({
    debug: true,
    fallbackLng: "en",

    // have a common namespace used around the full app
    ns: ["translations"],
    defaultNS: "translations",


    interpolation: {
      escapeValue: false, // not needed for react!!
    },

    resources: {
      en: {
        translations: translationEn
      },
      fr: {
        translations: translationFr
      },
    },

    react: {
      wait: true,
    },
  })

export default i18n